import type { IProductItem } from "@/interfaces";
import { reactive } from "vue";
import { defineStore } from "pinia";

export const useProductsStore = defineStore("products", () => {
	const products = reactive([
		{
			id: 1,
			name: "Shoes 1",
		},
		{
			id: 2,
			name: "Shoes 2",
		},
		{
			id: 3,
			name: "Shoes 3",
		},
		{
			id: 4,
			name: "Shoes 4",
		},
		{
			id: 5,
			name: "T-shirt 1",
		},
		{
			id: 6,
			name: "T-shirt 2",
		},
		{
			id: 7,
			name: "T-shirt 3",
		},
		{
			id: 8,
			name: "T-shirt 4",
		},
	]);

	const selected = reactive(new Set());

	const fillSelected = (item: IProductItem) => {
		if (selected.has(item)) {
			selected.delete(item);
			return;
		}
		if (selected.size < 6) {
			selected.add(item);
		}
	};

	return { products, selected, fillSelected };
});
